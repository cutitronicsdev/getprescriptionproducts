import logging
import json
import pymysql
import os

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def openConnection():
    """Generic MySQL DB connection handler"""
    global Connection
    try:
        Connection = pymysql.connect(os.environ['v_db_host'], os.environ['v_username'], os.environ['v_password'], os.environ['v_database'], connect_timeout=5)

    except Exception as e:
        logger.error(e)
        logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
        raise e


def fn_getPrescriptionProducts(lv_CutitronicsPresID):

    lv_list = []
    lv_statement = "SELECT CutitronicsPresHDRID, CutitronicsPresDETID, CutitronicsSKUCode, PrescriptionType, Frequency, FrequencyType, UsageAM, UsagePM, RecommendedAmount FROM BrandPrescriptionItems WHERE CutitronicsPresHDRID = %(CutitronicsPresID)s"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsPresID': lv_CutitronicsPresID})
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list.append(dict(zip(field_names, row)))
            if len(lv_list) == 0:
                return 400
            else:
                return lv_list
    except pymysql.err.IntegrityError as e:
        logger.error(e.args)
        return 500


def fn_checkPrescriptionExists(lv_CutitronicsPresID):

    lv_list = ""
    lv_statement = "SELECT CutitronicsPresID, CutitronicsBrandID, PrescriptionName, SkinType FROM BrandPrescriptions WHERE CutitronicsPresID = %(CutitronicsPresID)s"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsPresID': lv_CutitronicsPresID})
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = dict(zip(field_names, row))
            
            if len(lv_list) == 0:
                return 400
            else:
                return lv_list
    except pymysql.err.IntegrityError as e:
        logger.error(e.args)
        return 500


def fn_getProductName(lv_CutitronicsSKUCode):

    lv_list = ""
    lv_statement = "SELECT CutitronicsSKUName FROM BrandProducts WHERE CutitronicsSKUCode = %(CutitronicsSKUCode)s"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsSKUCode': lv_CutitronicsSKUCode})
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = dict(zip(field_names, row))
            if len(lv_list) == 0:
                return 400
            else:
                return lv_list.get('CutitronicsSKUName')
    except pymysql.err.IntegrityError as e:
        logger.error(e.args)
        return 500

def lambda_handler(event, context):
    """
    getAllTherapists

    """

    openConnection()

    logger.info("Lambda function - {function_name}".format(function_name=context.function_name))

    # Variables

    lv_CutitronicsPresID = None

    # Return block

    getPresProducts_out = {"Service": context.function_name, "Status": "Success", "ErrorCode": "", "ErrorDesc": "", "ErrorStack": "", "ServiceOutput": {"CutitronicsPresID": None, "PrescriptionName": None, "CutitronicsBrandID": None, "SkinType": None}}

    try:

        lv_Body = event.get('multiValueQueryStringParameters')

        logger.info(' ')
        logger.info("Payload body - '{body}'".format(body=lv_Body))
        logger.info(' ')

        lv_CutitronicsPresID = lv_Body.get('CutitronicsPresID')[0]

        '''
        1. Does brand exist ?
        '''

        logger.info(' ')
        logger.info("Testing if Prescription exists - '{lv_CutitronicsPresID}'".format(lv_CutitronicsPresID=lv_CutitronicsPresID))
        logger.info(' ')

        lv_Prescription = fn_checkPrescriptionExists(lv_CutitronicsPresID)

        if lv_Prescription == 500:  # Failure

            logger.error(' ')
            logger.error("Prescription '{lv_CutitronicsPresID}' has not been found in the back office systems".format(lv_CutitronicsPresID=lv_CutitronicsPresID))
            logger.error(' ')

            getPresProducts_out['Status'] = "Error"
            getPresProducts_out['ErrorCode'] = context.function_name + "_001"  # getAllTherapist_out_001
            getPresProducts_out['ErrorDesc'] = "Supplied prescription does not exist in the BO Database"
            getPresProducts_out["ServiceOutput"]["CutitronicsPresID"] = lv_CutitronicsPresID

            # Lambda response

            Connection.close()

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(getPresProducts_out)
            }

        '''
        2. Return Prescription Products
        '''
        logging.info("")
        logging.info("getting prescription products for presription '{lv_PresID}'".format(lv_PresID=lv_CutitronicsPresID))
        logging.info("")

        lv_PresProducts = fn_getPrescriptionProducts(lv_CutitronicsPresID)

        if lv_PresProducts == 400:  # Failure

            logger.error(' ')
            logger.error("No products found for prescription '{lv_CutitronicsPresID}'".format(lv_CutitronicsPresID=lv_CutitronicsPresID))
            logger.error(' ')

            getPresProducts_out['Status'] = "Success"
            getPresProducts_out['ErrorCode'] = context.function_name + "_002"  # getAllTherapist_out_001
            getPresProducts_out['ErrorDesc'] = "No products found for prescription '{lv_PresID}'".format(lv_PresID=lv_CutitronicsPresID)
            getPresProducts_out["ServiceOutput"]["CutitronicsPresID"] = lv_Prescription.get("CutitronicsPresID")
            getPresProducts_out["ServiceOutput"]["PrescriptionName"] = lv_Prescription.get("PrescriptionName")
            getPresProducts_out["ServiceOutput"]["CutitronicsBrandID"] = lv_Prescription.get("CutitronicsBrandID")
            getPresProducts_out["ServiceOutput"]["SkinType"] = lv_Prescription.get("SkinType")

            # Lambda response

            Connection.close()

            return {
                "isBase64Encoded": "false",
                "statusCode": 200,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(getPresProducts_out)
            }
        

        '''
        3. Build Output
        '''
        lv_OutList = []
        for lv_Product in lv_PresProducts:

            lv_Pres = {
                "CutitronicsPresHDRID": lv_Product.get("CutitronicsPresHDRID"),
                "CutitronicsPresDETID": lv_Product.get("CutitronicsPresDETID"),
                "CutitronicsSKUCode": lv_Product.get("CutitronicsSKUCode"),
                "CutitronicsSKUName": fn_getProductName(lv_Product.get("CutitronicsSKUCode")),
                "PrescriptionType": lv_Product.get("PrescriptionType"),
                "Frequency": lv_Product.get("Frequency"),
                "FrequencyType": lv_Product.get("FrequencyType"),
                "UsageAM": lv_Product.get("UsageAM"),
                "UsagePM": lv_Product.get("UsagePM"),
                "RecommendedAmount": lv_Product.get("RecommendedAmount")
            }
            lv_OutList.append(lv_Pres)


        # Create output

        getPresProducts_out['Status'] = "Success"
        getPresProducts_out['ErrorCode'] = context.function_name + "_000"
        getPresProducts_out['ErrorDesc'] = ""
        getPresProducts_out['ServiceOutput']['CutitronicsPresID'] = lv_CutitronicsPresID
        getPresProducts_out['ServiceOutput']['CutitronicsBrandID'] = lv_Prescription.get('CutitronicsBrandID')
        getPresProducts_out['ServiceOutput']['PrescriptionName'] = lv_Prescription.get('PrescriptionName')
        getPresProducts_out['ServiceOutput']['SkinType'] = lv_Prescription.get('SkinType')
        getPresProducts_out['ServiceOutput']['PresProducts'] = lv_OutList

        logger.info("")
        logger.info("Found prescription products, returning '{lv_FuncOut}'".format(lv_FuncOut=json.dumps(getPresProducts_out)))
        logger.info("")

        Connection.close()

        return {
            "isBase64Encoded": "false",
            "statusCode": 200,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(getPresProducts_out)
        }

    except Exception as e:
        logger.error("CATCH-ALL ERROR: Unexpected error: '{error}'".format(error=e))

        # Populate Cutitronics reply block

        getPresProducts_out['Status'] = "Error"
        getPresProducts_out['ErrorCode'] = context.function_name + "_000"  # regUser_000
        getPresProducts_out['ErrorDesc'] = "CatchALL"
        getPresProducts_out['ServiceOutput']['brandID'] = lv_CutitronicsPresID

        # Lambda response

        Connection.close()

        return {
            "isBase64Encoded": "false",
            "statusCode": 500,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(getPresProducts_out)
        }
